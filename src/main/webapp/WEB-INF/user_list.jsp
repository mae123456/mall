<%--
  Created by IntelliJ IDEA.
  User: Gao
  Date: 2022/12/26
  Time: 11:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@include file="header.jsp" %>
</head>
<body>
<table class="layui-hide" id="test" lay-filter="test"></table>

<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="add">添加</button>
        <button class="layui-btn layui-btn-sm" lay-event="deleteAll">批量删除</button>
    </div>
</script>

<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<script>
    layui.use('table', function(){
        var table = layui.table;

        table.render({
            elem: '#test'
            ,url:'/User/selectByPage'
            ,toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            ,defaultToolbar: ['filter', 'exports', 'print', { //自定义头部工具栏右侧图标。如无需自定义，去除该参数即可
                title: '提示'
                ,layEvent: 'LAYTABLE_TIPS'
                ,icon: 'layui-icon-tips'
            }]
            ,title: '用户数据表'
            ,cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'id', title:'ID'}
                ,{field:'name', title:'用户名'}
                ,{field:'password', title:'密码'}
                ,{field:'age', title:'年龄'}
                ,{fixed: 'right', title:'操作', toolbar: '#barDemo', width:150}
            ]]
            ,page: true
            ,id: 'tableId'
        });

        //头工具栏事件
        table.on('toolbar(test)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'add':
                    //location.href = '/user/getUserAddPage';
                    layer.open({
                        type: 2,
                        area: ['800px', '300px'],
                        content: '/getUserAddPage'
                    });
                    break;
                case 'deleteAll':
                    var data = checkStatus.data;
                    console.log(data);
                    // [{"id":8,"name":"3","age":3},{"id":9,"name":"4","age":4},{"id":10,"name":"55","age":55}]
                    var ids = [];
                    for (var i = 0; i < data.length; i++) {
                        ids.push(data[i].id);
                    }
                    //[2,3]
                    ids = ids.join(','); // '2,3'
                    layer.confirm('真的要删除么？', function(index) {
                        $.post(
                            '/User/deleteAll',
                            {'ids': ids},
                            function(jsonResult) {
                                console.log(jsonResult);
                                if (jsonResult.ok) {
                                    mylayer.okMsg("删除成功");
                                    //删除成功之后要重新刷新表格
                                    table.reload('tableId');
                                } else {
                                    mylayer.errorMsg("删除失败")
                                }
                            },
                            'json'
                        );
                        layer.close(index);
                    });
                    break;

                //自定义头工具栏右侧图标 - 提示
                case 'LAYTABLE_TIPS':
                    layer.alert('这是工具栏右侧自定义的一个图标按钮');
                    break;
            };
        });

        //监听行工具事件
        table.on('tool(test)', function(obj){
            var data = obj.data;
            console.log(data)
            if(obj.event === 'del'){
                layer.confirm('真的要删除么？', function(index) {
                    $.post(
                        '/user/deleteById',
                        {'id': data.id},
                        function(jsonResult) {
                            console.log(jsonResult);
                            if (jsonResult.ok) {
                                mylayer.okMsg("删除成功");
                                //删除成功之后要重新刷新表格
                                table.reload('tableId');
                            } else {
                                mylayer.errorMsg("删除失败")
                            }
                        },
                        'json'
                    );
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                layer.open({
                    type: 2,
                    area: ['800px', '300px'],
                    content: '/User/getUserUpdatePage?id='+data.id
                });
            }
        });
    });
</script>
</body>
</html>
