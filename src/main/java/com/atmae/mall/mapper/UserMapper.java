package com.atmae.mall.mapper;

import com.atmae.mall.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: Mae
 * @Date: 2022/12/26
 * @Time: 10:34
 * @Description:
 */
public interface UserMapper {

    List<User> selectAll();

    List<User> selectByPage(@Param("offset") int offset,@Param("limit") Integer limit);

    int selectTotalCount();

    int deleteById(Integer id);

    int deleteAll(Integer[] ids);

    int add(User user);

    User selectById(Integer id);

    int update(User user);
}
