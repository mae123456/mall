package com.atmae.mall.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: Mae
 * @Date: 2022/12/26
 * @Time: 14:15
 * @Description:
 */
@Controller
public class IndexController {
    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/getUserAddPage")
    public String getUserAddPage(){
        return "user_add";
    }



}
