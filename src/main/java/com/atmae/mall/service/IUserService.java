package com.atmae.mall.service;

import com.atmae.mall.pojo.User;
import com.atmae.mall.util.JSONResult;
import com.atmae.mall.util.LayUITableJSONResult;

import java.util.List;

/**
 * @Author: Mae
 * @Date: 2022/12/26
 * @Time: 10:33
 * @Description:
 */
public interface IUserService {
    List<User> selectAll();

    LayUITableJSONResult selectByPage(Integer page, Integer limit);

    JSONResult deleteById(Integer id);

    JSONResult deleteAll(Integer[] ids);

    JSONResult add(User user);

    User selectById(Integer id);

    JSONResult update(User user);
}
