package com.atmae.mall.service.impl;

import com.atmae.mall.mapper.UserMapper;
import com.atmae.mall.pojo.User;
import com.atmae.mall.service.IUserService;
import com.atmae.mall.util.JSONResult;
import com.atmae.mall.util.LayUITableJSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Mae
 * @Date: 2022/12/26
 * @Time: 10:33
 * @Description:
 */
@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;

    public List<User> selectAll() {
        List<User> users = userMapper.selectAll();
        return users;

    }

    @Override
    public LayUITableJSONResult selectByPage(Integer page, Integer limit) {
        int offset = (page - 1) * limit;
        List<User> users = userMapper.selectByPage(offset, limit);
        int totalCount = userMapper.selectTotalCount();
        return LayUITableJSONResult.ok(totalCount, users);
    }

    @Override
    public JSONResult deleteById(Integer id) {
        //影响的行数
        int count = userMapper.deleteById(id);
        return count == 1 ? JSONResult.ok("删除成功!") : JSONResult.error("删除失败!");
    }

    @Override
    public JSONResult deleteAll(Integer[] ids) {
        //影响的行数
        int count = userMapper.deleteAll(ids);
        return count == ids.length ? JSONResult.ok("删除成功!") : JSONResult.error("删除失败!");
    }

    @Override
    public JSONResult add(User user) {
        //影响的行数
        int count = userMapper.add(user);
        return count == 1 ? JSONResult.ok("添加成功!") : JSONResult.error("添加失败!");
    }

    @Override
    public User selectById(Integer id) {
       User user =userMapper.selectById(id);
        return  user;
    }

    @Override
    public JSONResult update(User user) {
       int count= userMapper.update(user);
        return count == 1 ? JSONResult.ok("更新成功!") : JSONResult.error("更新失败!");
    }
}
