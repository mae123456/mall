<%--
  Created by IntelliJ IDEA.
  User: Mae
  Date: 2022/12/26
  Time: 13:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@include file="header.jsp" %>
</head>
<body>

<form class="layui-form layui-form-pane" action="" id="formId">
    <input type="hidden" value="${user.id}" name="id">
    <div class="layui-form-item">
        <label class="layui-form-label">用户名</label>
        <div class="layui-input-block">
            <input type="text" name="name" value="${user.name}" autocomplete="off" placeholder="请输入标题" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-block">
            <input type="text" name="password"  value="${user.password}" autocomplete="off" placeholder="请输入标题" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">年龄</label>
        <div class="layui-input-inline">
            <input type="text" name="age"  value="${user.age}" lay-verify="required" placeholder="请输入" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <button class="layui-btn" type="button" onclick="submitForm()" lay-submit="" lay-filter="demo2">添加</button>
    </div>
</form>

<!-- 注意：项目正式环境请勿引用该地址 -->
<script src="//unpkg.com/layui@2.7.6/dist/layui.js"></script>

<script>
    function submitForm() {
        $.post(
            '/User/update',
            $('#formId').serialize(), //{'name'='zhangsan','password'=12312,age=5}
            function (jsonResult) {
                if (jsonResult.ok) {
                    //mylayer.okMsg("添加成功!");
                    // //删除成功重新刷新表格
                    // table.reload('tableId')
                    var index = parent.layer.getFrameIndex(window.name);
                    layer.msg(
                        '更新成功',
                        {icon: 1, time: 1000},
                        function () {
                            parent.layer.close(index);
                            window.parent.location.reload()
                        }
                    )
                } else {
                    mylayer.errorMsg("更新失败!");
                }
            },
            'json'
        )
    }
</script>
</body>
</html>
