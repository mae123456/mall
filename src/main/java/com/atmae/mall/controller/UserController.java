package com.atmae.mall.controller;

import com.atmae.mall.pojo.User;
import com.atmae.mall.service.IUserService;
import com.atmae.mall.util.JSONResult;
import com.atmae.mall.util.LayUITableJSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Mae
 * @Date: 2022/12/26
 * @Time: 10:08
 * @Description:
 */
@RequestMapping("/User")
@Controller
public class UserController {
    @Autowired
    private IUserService userService;

    @ResponseBody
    @GetMapping("/selectAll")
    public List<User> selectAll() {
        List<User> users = userService.selectAll();
        return users;
    }

    @GetMapping("/selectAll2")
    public String selectAll(Model model) {
        List<User> users = userService.selectAll();
        model.addAttribute("list", users);
        return "user_list";
//        return "/WEB-INF/user_list.jsp";
    }


    @ResponseBody
    @GetMapping("/selectByPage")
    public LayUITableJSONResult selectByPage(Integer page, Integer limit) {
        LayUITableJSONResult result = userService.selectByPage(page, limit);
        return result;
    }


    @ResponseBody
    @GetMapping("/deleteById")
    public JSONResult selectByPage(Integer id) {
        JSONResult result = userService.deleteById(id);
        return result;
    }

    @ResponseBody
    @GetMapping("/deleteAll")
    public JSONResult deleteAll(Integer []ids){
        JSONResult result =userService.deleteAll(ids);
       return result;
    }

    @ResponseBody
    @PostMapping("/add")
    public JSONResult add(User user){
        JSONResult result=userService.add(user);
        return result;
    }

    @GetMapping("/getUserUpdatePage")
    public String getUserUpdatePage(Integer id,Model model){
        User user=userService.selectById(id);
        model.addAttribute("user",user);
        return "user_update";
    }

    @ResponseBody
    @PostMapping("/update")
    public JSONResult update(User user){
        JSONResult result= userService.update(user);
        return  result;
    }

}
